| Name | data size (e.g. RAM) | code size (e.g. Flash) |
| --- | --- | --- |
| Class 0, C0 | << 10 KiB | << 100 KiB |
| Class 1, C1 | ~ 10 KiB | ~ 100 KiB |
| Class 2, C2 | ~ 50 KiB | ~ 250 KiB |

Table: This table lists the different classes of constrained devices defined in RFC 7228.
