#!/usr/bin/env node
const pandoc = require('pandoc-filter');

/* TODOs:
- JSDoc comments
- label with unique id
- Jest Test
- npm publish einrichten
- CI Prettier
- CI Jest
- CI npm publish
- löschen der temp dateien
*/

function latex(value) {
  return pandoc.RawBlock('latex', value);
}
function inlatex(value) {
  return pandoc.RawInline('latex', value);
}

function inlatexBoldText(values) {
  return [inlatex('\\textbf{'), ...values, inlatex('}')];
}

function tblCaption(values) {
  return pandoc.Para([inlatex('\\caption{'), ...values, inlatex('}')]);
}

function tblAlignment(values) {
  const alignments = {
    AlignDefault: 'l',
    AlignLeft: 'l',
    AlignCenter: 'c',
    AlignRight: 'r',
  };
  /*
  'values' looks like this: [
          { "t": "AlignDefault" },
          { "t": "AlignCenter" },
          { "t": "AlignRight" }
        ]
  */
  return `| ${values.map((value) => alignments[value['t']]).join(' | ')} |`;
}

function tblHeaders(values) {
  /* 'values' looks like this
  [
    [
      {
        "t": "Plain",
        "c": [
          { "t": "Str", "c": "data" },
          { "t": "Space" },
          { "t": "Str", "c": "size" }
        ]
      }
    ],
    ...
  ]
  */

  let result = [];
  // flatten the original array because it will become Para
  values.forEach((headerElement, index, array) => {
    // empty header are possible
    if (headerElement.length == 1) {
      result = result.concat(inlatexBoldText(headerElement[0]['c']));
    }
    // add the column separator
    if (index !== array.length - 1) {
      result.push(inlatex(' & '));
    }
  });
  result = result.concat(inlatex('\\\\ \n\\hline \n'));

  return pandoc.Para(result);
}

function tblContent(tableRows) {
  let result = [];
  tableRows.forEach((row) => {
    let entries = [];
    row.forEach((entry) => {
      // empty entries are possible
      if (entry.length == 1) {
        entries = entries.concat(entry[0]['c']);
      }
      entries.push(inlatex(' & '));
    });
    //delete last '&'
    entries.pop();
    result = result.concat(entries);
    result.push(inlatex('\\\\ \n\\hline \n'));
  });
  return pandoc.Para(result);
}

function action(type, value, format, meta) {
  if (type === 'Table') {
    return [
      latex('\\begin{table}[htbp]\n\\centering\n'),
      tblCaption(value[0]),
      latex(`\\begin{tabular}{${tblAlignment(value[1])}}\n\\hline`),
      tblHeaders(value[3]),
      tblContent(value[4]),
      latex('\\end{tabular}'),
      latex('\\label{tab:TODONameRef}'),
      latex('\\end{table}'),
    ];
  }
}
pandoc.toJSONFilter(action);
